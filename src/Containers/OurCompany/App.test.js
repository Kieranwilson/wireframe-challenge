import React from 'react';
import ReactDOM from 'react-dom';
import OurCompany from './OurCompany';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<OurCompany />, div);
  ReactDOM.unmountComponentAtNode(div);
});
