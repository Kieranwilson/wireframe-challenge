import { connect } from 'react-redux';
import OurPeople from './OurCompany';
import { getCompanyData } from 'Store/CompanyData';

const mapStateToProps = state => ({
  companyData: state.companyData.data,
  error: state.companyData.error
});

const mapDispatchToProps = { getCompanyData };

const OurPeopleContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(OurPeople);

export default OurPeopleContainer;
