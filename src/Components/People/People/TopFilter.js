import React from 'react';
import style from './style.module.scss';

export default class TopFilter extends React.PureComponent {
  render() {
    const { filter, search, handleFilterChange, handleSearchChange } = this.props;
    return (
      <div className={style.above}>
        <h2 className={style.header}>Our Employees</h2>
        <div className={style.filter}>
          <div className={style.label}>Sort by:</div>
          <div className={style.input}>
            <select value={filter} onChange={handleFilterChange}>
              <option value="firstName">First Name</option>
              <option value="lastName">Last Name</option>
              <option value="jobTitle">Job Title</option>
              <option value="age">Age</option>
            </select>
          </div>

          <div className={style.label}>Search:</div>
          <div className={style.input}>
            <input type="text" value={search} onChange={handleSearchChange} />
          </div>
        </div>
      </div>
    );
  }
}
