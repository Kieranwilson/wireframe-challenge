import React from 'react';
import Cards from 'Components/Cards/Cards';
import Card from 'Components/Cards/Card';
import PersonShort from '../PersonShort';
import TopFilter from './TopFilter';
import style from './style.module.scss';
import PersonLong from '../PersonLong';
import Modal from '../../Modal';

export default class People extends React.PureComponent {
  state = {
    active: null,
    filter: 'firstName',
    search: ''
  };

  filterChange = event => this.setState({ filter: event.target.value });
  searchChange = event => this.setState({ search: event.target.value });

  highlight = person => this.setState({ active: person });
  closeModal = () => this.setState({ active: null });

  filterSort() {
    const {
      props: { people },
      state: { filter, search }
    } = this;
    return people
      .filter(person =>
        `${person.firstName} ${person.lastName}`.toLowerCase().includes(search.toLowerCase())
      )
      .sort((a, b) => (a[filter] > b[filter] ? 1 : -1));
  }

  render() {
    const {
      props: { people },
      filterChange,
      searchChange,
      highlight,
      closeModal,
      state: { filter, search, active }
    } = this;
    return (
      <div className={style.people}>
        <Modal open={!!active} onClickOutside={closeModal} showClose onClickClose={closeModal}>
          <PersonLong {...active} />
        </Modal>
        <TopFilter
          handleFilterChange={filterChange}
          handleSearchChange={searchChange}
          filter={filter}
          search={search}
        />
        <Cards>
          {this.filterSort().map(person => (
            <Card key={person.id}>
              <PersonShort {...person} highlight={highlight} />
            </Card>
          ))}
        </Cards>
      </div>
    );
  }
}
