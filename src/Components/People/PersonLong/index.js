import React from 'react';
import cx from 'classnames';
import style from './style.module.scss';

export default class PersonLong extends React.PureComponent {
  render() {
    const { className, avatar, firstName, lastName, bio, jobTitle, age, dateJoined } = this.props;
    return (
      <div className={cx(style.person, className)}>
        <div className={style.left}>
          <div className={style.avatar}>
            <img src={avatar} alt={firstName} />
          </div>
          <span className={style.title}>{jobTitle}</span>
          <span>{age}</span>
          <span>{new Date(dateJoined).toLocaleDateString()}</span>
        </div>
        <div className={style.right}>
          <div className={style.name}>
            <span>
              {firstName} {lastName}
            </span>
          </div>
          <p>{bio}</p>
        </div>
      </div>
    );
  }
}
