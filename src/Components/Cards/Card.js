import React from 'react';
import cx from 'classnames';
import style from './Card.module.scss';

const Card = ({ children, className }) => (
  <div className={cx(style.card, className)}>{children}</div>
);

export default Card;
