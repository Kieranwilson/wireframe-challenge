import {
  FETCH_COMPANYDATA_ERROR,
  FETCH_COMPANYDATA_FINISH,
  FETCH_COMPANYDATA_START
} from './types';
import axios from 'axios';

export const getCompanyData = () => async dispatch => {
  dispatch({
    type: FETCH_COMPANYDATA_START
  });
  try {
    const { data } = await axios.get('/data/company-data.json');
    dispatch({
      type: FETCH_COMPANYDATA_FINISH,
      data
    });
  } catch (error) {
    dispatch({
      type: FETCH_COMPANYDATA_ERROR,
      error
    });
  }
};
