import * as Types from './types';

export const initialState = {
  data: null,
  pending: false,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_COMPANYDATA_START:
      return {
        ...state,
        pending: true
      };
    case Types.FETCH_COMPANYDATA_FINISH:
      return {
        ...state,
        pending: false,
        data: action.data
      };
    case Types.FETCH_COMPANYDATA_ERROR:
      return { ...state, pending: false, error: action.error };
    default:
      return state;
  }
};

export default reducer;
