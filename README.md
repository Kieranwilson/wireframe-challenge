## Technical Requirements

You must have installed:
- Node 8.x
- NPM or Yarn

The goal of this test is for us to get a sense of your JavaScript skill. We prefer seeing this demo app be implemented using React and Redux but all JavaScript frameworks will be considered.

## Instructions

1. Read this readme
2. Ensure you have understood the wireframes
3. Your commit messages should follow the documented [Commit Message Conventions]((https://gist.github.com/stephenparish/9941e89d80e2bc58a153)). Ensure you commit regularly so we can follow your step by step progress throughout the application.
4. Use a sample set of data
5. Design your own components and interactions. The use of third-party plugins for this simple demo app should be kept to a minimum as the wireframes have been designed to be achievable using the chosen framework/library without any additional plugins. Feel free to use helper libraries such as `lodash` or similar.

## The Application Requirements

This simple employee roster app will use a static `sample-data.json` file with randomly generated sample data that should be used to create a simple one-page app of the roster for the company's employees.

Candidates should show their reactjs development / design skills which align to industrial standard in this code test (ie. use ajax properly to retrieve data from sample-data.json)

The roster should be represented in a "card" layout which initially shows minimal employee information consisting of the employee's name, avatar picture and a truncated excerpt of their bio.

Clicking on a card should highlight the card in some way and render additional information in a modal overlay (see wireframe `detail-view.png`). Clicking the close (X) icon above the modal should close the modal and remove the highlight from the card that was clicked. Clicking anywhere outside the modal's content should also close the modal.

Your application should render correctly in a mobile sized device.

## Wireframes

![Grid View](/public/grid-view.png)
![Detail View](/public/detail-view.png)
